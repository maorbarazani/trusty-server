export enum Region {
  All = 'ALL',
  North = 'NORTH',
  Center = 'CENTER',
  South = 'SOUTH',
}
