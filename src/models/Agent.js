"use strict";
exports.__esModule = true;
var mongoose_1 = require("mongoose");
var Location_1 = require("./Location");
var AgentSchema = new mongoose_1.Schema({
    name: String,
    description: String,
    imageUrl: String,
    price: Number,
    location: Location_1.Location,
    rating: Number,
    timesRated: Number,
});
exports["default"] = mongoose_1.model('Agent', AgentSchema);
