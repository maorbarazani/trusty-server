"use strict";
exports.__esModule = true;
var Region;
(function (Region) {
    Region["All"] = "ALL";
    Region["North"] = "NORTH";
    Region["Center"] = "CENTER";
    Region["South"] = "SOUTH";
})(Region = exports.Region || (exports.Region = {}));
