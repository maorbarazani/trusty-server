import {Schema, model} from 'mongoose';
import {Location} from "./Location";

let AgentSchema: Schema = new Schema({

    name: String,
    description: String,
    imageUrl: String,
    price: Number,
    location: Location,
    rating: Number,
    timesRated: Number,

});

export default model('Agent', AgentSchema);



