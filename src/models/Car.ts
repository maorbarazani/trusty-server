import {Schema, model} from 'mongoose';

let CarSchema: Schema = new Schema({

    maker: String,
    model: String,
    year: Number,
    color: String

});

export default model('Car', CarSchema);
