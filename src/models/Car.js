"use strict";
exports.__esModule = true;
var mongoose_1 = require("mongoose");
var CarSchema = new mongoose_1.Schema({
    maker: String,
    model: String,
    year: Number,
    color: String
});
exports["default"] = mongoose_1.model('Car', CarSchema);
