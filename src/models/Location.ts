import {Region} from './Region';

export class Location {
  name: string;
  region: Region;
}
