import * as express from 'express';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as cors from 'cors';

// import routers
import CarRouter from "./controllers/CarController";


// Server class
class Server {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    public config(): void {
        // set up mongoose
        const MONGO_URI = 'mongodb://maor:q1w2e3r4@ds125574.mlab.com:25574/trusty';
        mongoose.connect(MONGO_URI || process.env.MONGODB_URI);

        // config
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(helmet());
        this.app.use(logger('dev'));
        this.app.use(compression());
        this.app.use(cors());
    }

    public routes(): void {
        let router: express.Router;
        router = express.Router();

        this.app.use('/', router);
        this.app.use('/api/cars', CarRouter);

    }
}

export default new Server().app;
