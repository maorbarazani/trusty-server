import {IBaseDao} from "./interfaces/IBaseDao";
import Car from "../models/Car";

export class BaseDao<T> implements IBaseDao<T> {
    public create(item: T): Promise<boolean> {
        return undefined;
    }

    public delete(id: string): Promise<boolean> {
        return undefined;
    }

    public update(id: string, item: T): Promise<boolean> {
        return undefined;
    }

    public find(item: T): Promise<T[]> {
        return item.find({})
            .then((data) => {
                res.json(data);
            })
            .catch((err) => {
                res.json(err);
            })
    }

    public findOne(id: string): Promise<T> {
        return undefined;
    }

}