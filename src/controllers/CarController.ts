import {Router, Request, Response, NextFunction} from 'express';
import Car from "../models/Car";
import {BaseDao} from "../DAO/BaseDao";

class CarController {

    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public getCars(req: Request, res: Response): void {

    }

    // public getCars(req: Request, res: Response): void {
    //
    //     Car.find({})
    //         .then((data) => {
    //             res.json(data);
    //         })
    //         .catch((err) => {
    //             res.json(err);
    //         })
    // }

    public getCarsByMaker(req: Request, res: Response): void {
        const maker = req.params.maker;
        Car.find({maker: maker})
            .then((data) => {
                res.json(data);
            })
            .catch((err) => {
                res.json(err);
            })
    }

    public createCar(req: Request, res: Response): void {

    }

    public updateCar(req: Request, res: Response): void {

    }

    public deleteCar(req: Request, res: Response): void {

    }

    routes() {
        this.router.get('/', this.getCars);
        this.router.get('/:maker', this.getCarsByMaker);
    }
}

//export
const carRoutes = new CarController();
carRoutes.routes();

export default carRoutes.router;
