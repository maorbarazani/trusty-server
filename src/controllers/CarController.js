"use strict";
exports.__esModule = true;
var express_1 = require("express");
var Car_1 = require("../models/Car");
var CarController = /** @class */ (function () {
    function CarRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    CarRouter.prototype.getCars = function (req, res) {
        Car_1["default"].find({})
            .then(function (data) {
            var status = res.statusCode;
            res.json({ status: status, data: data });
        })["catch"](function (err) {
            var status = res.statusCode;
            res.json({ status: status, err: err });
        });
    };
    CarRouter.prototype.getCar = function (req, res) {
    };
    CarRouter.prototype.createCar = function (req, res) {
    };
    CarRouter.prototype.updateCar = function (req, res) {
    };
    CarRouter.prototype.deleteCar = function (req, res) {
    };
    CarRouter.prototype.routes = function () {
        this.router.get('/', this.getCars);
    };
    return CarRouter;
}());
//export
var carRoutes = new CarController();
carRoutes.routes();
exports["default"] = carRoutes.router;
