"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Car_1 = require("../models/Car");
var CarRouter = /** @class */ (function () {
    function CarRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    CarRouter.prototype.getCars = function (req, res) {
        Car_1.default.find({})
            .then(function (data) {
            res.json(data);
        })
            .catch(function (err) {
            res.json(err);
        });
    };
    CarRouter.prototype.getCarsByMaker = function (req, res) {
        var maker = req.params.maker;
        Car_1.default.find({ maker: maker })
            .then(function (data) {
            res.json(data);
        })
            .catch(function (err) {
            res.json(err);
        });
    };
    CarRouter.prototype.createCar = function (req, res) {
    };
    CarRouter.prototype.updateCar = function (req, res) {
    };
    CarRouter.prototype.deleteCar = function (req, res) {
    };
    CarRouter.prototype.routes = function () {
        this.router.get('/', this.getCars);
        this.router.get('/:maker', this.getCarsByMaker);
    };
    return CarRouter;
}());
//export
var carRoutes = new CarRouter();
carRoutes.routes();
exports.default = carRoutes.router;
//# sourceMappingURL=CarRouter.js.map