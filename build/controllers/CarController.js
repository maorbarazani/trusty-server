"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Car_1 = require("../models/Car");
var CarController = /** @class */ (function () {
    function CarController() {
        this.router = express_1.Router();
        this.routes();
    }
    CarController.prototype.getCars = function (req, res) {
    };
    // public getCars(req: Request, res: Response): void {
    //
    //     Car.find({})
    //         .then((data) => {
    //             res.json(data);
    //         })
    //         .catch((err) => {
    //             res.json(err);
    //         })
    // }
    CarController.prototype.getCarsByMaker = function (req, res) {
        var maker = req.params.maker;
        Car_1.default.find({ maker: maker })
            .then(function (data) {
            res.json(data);
        })
            .catch(function (err) {
            res.json(err);
        });
    };
    CarController.prototype.createCar = function (req, res) {
    };
    CarController.prototype.updateCar = function (req, res) {
    };
    CarController.prototype.deleteCar = function (req, res) {
    };
    CarController.prototype.routes = function () {
        this.router.get('/', this.getCars);
        this.router.get('/:maker', this.getCarsByMaker);
    };
    return CarController;
}());
//export
var carRoutes = new CarController();
carRoutes.routes();
exports.default = carRoutes.router;
//# sourceMappingURL=CarController.js.map