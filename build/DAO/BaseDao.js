"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BaseDao = /** @class */ (function () {
    function BaseDao() {
    }
    BaseDao.prototype.create = function (item) {
        return undefined;
    };
    BaseDao.prototype.delete = function (id) {
        return undefined;
    };
    BaseDao.prototype.update = function (id, item) {
        return undefined;
    };
    BaseDao.prototype.find = function (item) {
        return item.find({})
            .then(function (data) {
            res.json(data);
        })
            .catch(function (err) {
            res.json(err);
        });
    };
    BaseDao.prototype.findOne = function (id) {
        return undefined;
    };
    return BaseDao;
}());
exports.BaseDao = BaseDao;
//# sourceMappingURL=BaseDao.js.map