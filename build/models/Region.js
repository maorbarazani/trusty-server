"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Region;
(function (Region) {
    Region["All"] = "ALL";
    Region["North"] = "NORTH";
    Region["Center"] = "CENTER";
    Region["South"] = "SOUTH";
})(Region = exports.Region || (exports.Region = {}));
//# sourceMappingURL=Region.js.map